#!/bin/sh

export DIRNAME="$(dirname "$(readlink -f "${0}")")"

# Install prerequisites
sudo add-apt-repository ppa:ehoover/compholio
sudo apt-get update
sudo apt-get -y install libselinux1-dev libfuse-dev libglib2.0-dev xorriso elfres

